package digitalhanji;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class BorderRectangle {
	private Rectangle rect;
	BorderRectangle(int l, int h){
		this.rect = new Rectangle(l, h);
		this.rect.setFill(Color.BLACK);
	}
	
	Rectangle getBorder(){
		return this.rect;
	}
}
